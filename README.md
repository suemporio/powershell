1. Replacement script: edit a file with name passed as an environment variable - remove all lines that contain a substring passed as a parameter
2. Get a process name by ID or ID by name, delete a process by name or ID
3. Check if a port is opened locally, get name of the process that opened a specified port
4. Create a random password with letters and digits with specified length
5. Change value of a property in a JSON file
6. Get sizes of files whose names contain a specified substring
